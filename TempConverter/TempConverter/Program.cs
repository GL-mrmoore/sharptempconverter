﻿using System;

namespace TempConverter
{
    class Program
    {
        static void Main(string[] args)
        {
            Console.WriteLine("What is the temperature in degrees Fahrenheit?");
            double fahrenheit = double.Parse(Console.ReadLine());

            double celsius = (fahrenheit - 32) * 5 / 9;
            Console.WriteLine("The temperature in degrees Celsius is: " + Math.Round(celsius, 2));
        }
    }
}
